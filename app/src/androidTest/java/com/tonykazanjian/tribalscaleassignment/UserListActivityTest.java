package com.tonykazanjian.tribalscaleassignment;

import android.app.ListActivity;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import com.tonykazanjian.tribalscaleassignment.user_list.UserListActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

/**
 * @author Tony Kazanjian
 */

public class UserListActivityTest {

    private ActivityIdlingResource mActivityIdlingResource;

    @Rule
    public ActivityTestRule<UserListActivity> mActivityTestRule = new ActivityTestRule<>(UserListActivity.class);

    @Before
    public void setUp ()  {
        registerIdlingResource();
    }

    @After
    public void tearDown () {
        unregisterIdlingResource();
    }

    public void registerIdlingResource() {
        mActivityIdlingResource = (ActivityIdlingResource) BaseActivity.getIdlingResource();
        Espresso.registerIdlingResources(mActivityIdlingResource);
    }

    public void unregisterIdlingResource() {
        if (mActivityIdlingResource != null) {
            Espresso.unregisterIdlingResources(mActivityIdlingResource);
        }
    }
}

package com.tonykazanjian.tribalscaleassignment;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.LinearLayout;

import com.tonykazanjian.tribalscaleassignment.user_detail.UserDetailActivity;
import com.tonykazanjian.tribalscaleassignment.user_list.UserListActivity;

import org.hamcrest.Description;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Tony Kazanjian
 */
@RunWith(AndroidJUnit4.class)
public class DisplayDetailTest extends UserListActivityTest {

    @Rule // Keeps track of the intents launching
    public IntentsTestRule<UserListActivity> mListActivityRule = new IntentsTestRule<>(UserListActivity.class);

    @Test
    public void launchUserDetailActivity() throws Exception {
        onView(withId(R.id.users_rv))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        BoundedMatcher<View, LinearLayout> buildingContainerMatcher = new BoundedMatcher<View, LinearLayout>(LinearLayout.class) {
            @Override
            protected boolean matchesSafely(LinearLayout item) {
                return item.isShown();
            }

            @Override
            public void describeTo(Description description) {

            }
        };

        onView(withId(R.id.user_detail_image)).check(matches(buildingContainerMatcher));

        // This checks the intent against the current activity to see if the expected intent was launched
        intended(hasComponent(UserDetailActivity.class.getName()));

    }

    @Test
    public void getUsers(){
        onView(withId(R.id.user_name)).check(matches(isDisplayed()));
    }
}

package com.tonykazanjian.tribalscaleassignment;

import com.tonykazanjian.tribalscaleassignment.model.Location;
import com.tonykazanjian.tribalscaleassignment.model.Name;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * @author Tony Kazanjian
 */

public class MockAPIService implements RandomApiService {

    private final BehaviorDelegate<RandomApiService> mBehaviorDelegate;

    private UserResponse mMockUserResponse;

    public MockAPIService(BehaviorDelegate<RandomApiService> behaviorDelegate) {
        mBehaviorDelegate = behaviorDelegate;
        mMockUserResponse = new UserResponse();
    }

    public static MockRetrofit createMockServer () {
        return new MockRetrofit.Builder(
                new Retrofit.Builder().baseUrl("https://fakeUrl.com")
                        .addConverterFactory(GsonConverterFactory.create()).build())
                .networkBehavior(NetworkBehavior.create())
                .build();
    }

    @Override
    public Call<UserResponse> getUsers() {
        UserResponse userResponse = mMockUserResponse;
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setName(new Name("Mr.", "Random", "User"));
            user.setGender("Male");
            user.setEmail("random.person@gmail.com");
            user.setBirthday("October 8th, 1981");
            user.setLocation(new Location("1234 User St", "RandomCity", "CA", 11111));
            userList.add(i, user);
        }
        userResponse.setUserList(userList);
        return mBehaviorDelegate.returningResponse(userResponse).getUsers();
    }
}

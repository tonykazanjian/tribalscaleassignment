package com.tonykazanjian.tribalscaleassignment.user_list;

import com.tonykazanjian.tribalscaleassignment.MockRetrofitTest;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiServiceGenerator;
import com.tonykazanjian.tribalscaleassignment.utils.Constants;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Tony Kazanjian
 */
@RunWith(MockitoJUnitRunner.class)
public class UserListPresenterTest {

    private UserListPresenter mUserListPresenter;
    private MockRetrofitTest mMockRetrofitTest;

    @Mock
    private UserListView mUserListView;

    @Before
    public void setUp() throws Exception {
        mUserListPresenter = new UserListPresenter(mUserListView);
        mMockRetrofitTest = new MockRetrofitTest();
        mMockRetrofitTest.testRandomUserRetrieval();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void onMockUserRequest() throws Exception{
        mUserListPresenter.displayUsers(mMockRetrofitTest.getUserResponse());
        Mockito.verify(mUserListView).onUserListDisplayed(mMockRetrofitTest.getUserResponse());
    }

    @Test
    public void onMockUserClicked() throws Exception {
        User user = mMockRetrofitTest.getUserResponse().getUserList().get(0);
        mUserListPresenter.selectUser(user);
        Mockito.verify(mUserListView).onUserItemClicked(user);

    }

}
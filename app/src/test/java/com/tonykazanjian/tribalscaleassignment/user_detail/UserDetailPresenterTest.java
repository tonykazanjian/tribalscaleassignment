package com.tonykazanjian.tribalscaleassignment.user_detail;

import com.tonykazanjian.tribalscaleassignment.MockRetrofitTest;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiServiceGenerator;
import com.tonykazanjian.tribalscaleassignment.user_list.UserListPresenter;
import com.tonykazanjian.tribalscaleassignment.user_list.UserListPresenterTest;
import com.tonykazanjian.tribalscaleassignment.utils.Constants;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.junit.Assert.*;

/**
 * @author Tony Kazanjian
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDetailPresenterTest {

    private UserDetailPresenter mUserDetailPresenter;
    private MockRetrofitTest mMockRetrofitTest;

    @Mock
    private UserDetailView mUserDetailView;

    @Before
    public void setUp() throws Exception {
        mUserDetailPresenter = new UserDetailPresenter(mUserDetailView);
        mMockRetrofitTest = new MockRetrofitTest();
        mMockRetrofitTest.testRandomUserRetrieval();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void displayMockData() throws Exception {
        User user = mMockRetrofitTest.getUserResponse().getUserList().get(0);
        mUserDetailPresenter.displayUserData(user);
        Mockito.verify(mUserDetailView).onDetailDisplayed(user);
    }
}
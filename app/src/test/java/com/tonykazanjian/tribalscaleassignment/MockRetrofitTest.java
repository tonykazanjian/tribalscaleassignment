package com.tonykazanjian.tribalscaleassignment;

import com.tonykazanjian.tribalscaleassignment.model.UserResponse;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;
import com.tonykazanjian.tribalscaleassignment.utils.Constants;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;

/**
 * @author Tony Kazanjian
 */
@RunWith(MockitoJUnitRunner.class)
public class MockRetrofitTest {
    private MockRetrofit mockRetrofit;
    private UserResponse mUserResponse;

    @Before
    public void setUp() throws Exception {
        mockRetrofit = MockAPIService.createMockServer();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testRandomUserRetrieval() throws Exception {

        // For using this method in other tests
        if (mockRetrofit == null) {
            mockRetrofit = MockAPIService.createMockServer();
        }

        BehaviorDelegate<RandomApiService> delegate = mockRetrofit.create(RandomApiService.class);
        RandomApiService mockAPIService = new MockAPIService(delegate);

        //Actual Test
        Call<UserResponse> userResponseCall = mockAPIService.getUsers();
        Response<UserResponse> userResponse = userResponseCall.execute();

        //Asserting response
        int userSize = Integer.parseInt(Constants.QUERY_USER_SIZE);
        mUserResponse = userResponse.body();
        Assert.assertTrue(mUserResponse.getUserList().size() == userSize);
    }

    public UserResponse getUserResponse(){
        return mUserResponse;
    }

}

package com.tonykazanjian.tribalscaleassignment.user_list;

import android.content.Context;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tonykazanjian.tribalscaleassignment.R;
import com.tonykazanjian.tribalscaleassignment.model.Picture;
import com.tonykazanjian.tribalscaleassignment.model.User;

import java.util.List;

/**
 * @author Tony Kazanjian
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    List<User> mUserList;
    UserListPresenter mUserListPresenter;

    public UserListAdapter(List<User> userList, UserListPresenter userListPresenter) {
        mUserList = userList;
        mUserListPresenter = userListPresenter;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        final Context context = holder.itemView.getContext();

        User user = mUserList.get(position);
        VectorDrawableCompat placeholderDrawable = VectorDrawableCompat.create(context.getResources(), R.drawable.placeholder_user_image, null);

        if (user.getPicture()!= null){

            Picture picture = user.getPicture();

            Transformation transformation = new RoundedTransformationBuilder()
                    .cornerRadiusDp(60)
                    .oval(false)
                    .build();

            Picasso.with(context).load(picture.getThumbnailUrl())
                    .placeholder(placeholderDrawable)
                    .fit().centerCrop()
                    .transform(transformation)
                    .into(holder.mUserImage);
        } else {
            holder.mUserImage.setImageDrawable(placeholderDrawable);
        }

        holder.mUserName.setText(context.getResources().getString(R.string.user_name, user.getName().getTitle(),
                user.getName().getFirst(), user.getName().getLast()));

        holder.itemView.setOnClickListener(view -> mUserListPresenter.selectUser(user));
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        ImageView mUserImage;
        TextView mUserName;

        public UserHolder(View itemView) {
            super(itemView);
            mUserImage = (ImageView)itemView.findViewById(R.id.user_image);
            mUserName = (TextView)itemView.findViewById(R.id.user_name);
        }
    }
}

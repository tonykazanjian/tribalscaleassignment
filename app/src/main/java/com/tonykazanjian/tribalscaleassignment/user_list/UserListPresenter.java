package com.tonykazanjian.tribalscaleassignment.user_list;

import com.tonykazanjian.tribalscaleassignment.BaseActivity;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;

import java.util.Comparator;
import java.util.List;

/**
 * @author Tony Kazanjian
 */

public class UserListPresenter {

    UserListView mUserListView;

    public UserListPresenter(UserListView userListView) {
        mUserListView = userListView;
    }

    public void displayUsers(UserResponse userResponse){
        mUserListView.onUserListDisplayed(userResponse);
        BaseActivity.setActivityIdle();
    }

    public void selectUser(User user){
        mUserListView.onUserItemClicked(user);
    }
}

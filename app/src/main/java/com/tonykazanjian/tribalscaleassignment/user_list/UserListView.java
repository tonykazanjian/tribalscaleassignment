package com.tonykazanjian.tribalscaleassignment.user_list;

import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;

/**
 * @author Tony Kazanjian
 */

public interface UserListView {
    void onUserListDisplayed(UserResponse userResponse);
    void onUserItemClicked(User user);
}

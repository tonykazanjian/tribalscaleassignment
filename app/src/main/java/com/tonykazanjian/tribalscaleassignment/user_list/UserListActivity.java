package com.tonykazanjian.tribalscaleassignment.user_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tonykazanjian.tribalscaleassignment.BaseActivity;
import com.tonykazanjian.tribalscaleassignment.R;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiServiceGenerator;
import com.tonykazanjian.tribalscaleassignment.user_detail.UserDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListActivity extends BaseActivity implements UserListView{

    private UserListPresenter mUserListPresenter;
    private UserListAdapter mUserListAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        mUserListPresenter = new UserListPresenter(this);

        mRecyclerView = (RecyclerView)findViewById(R.id.users_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        fetchUsers();
    }

    private void fetchUsers(){
        RandomApiService service = RandomApiServiceGenerator.createService();

        Call<UserResponse> call = service.getUsers();
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    mUserListPresenter.displayUsers(response.body());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onUserListDisplayed(UserResponse userResponse) {
        mUserListAdapter = new UserListAdapter(userResponse.getUserList(), mUserListPresenter);
        mRecyclerView.setAdapter(mUserListAdapter);
    }

    @Override
    public void onUserItemClicked(User user) {
        startActivity(UserDetailActivity.newIntent(this, user));

    }
}

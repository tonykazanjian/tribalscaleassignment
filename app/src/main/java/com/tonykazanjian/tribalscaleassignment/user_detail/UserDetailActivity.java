package com.tonykazanjian.tribalscaleassignment.user_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tonykazanjian.tribalscaleassignment.BaseActivity;
import com.tonykazanjian.tribalscaleassignment.R;
import com.tonykazanjian.tribalscaleassignment.model.Picture;
import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.utils.FormatUtils;

/**
 * @author Tony Kazanjian
 */

public class UserDetailActivity extends BaseActivity implements UserDetailView {

    UserDetailPresenter mUserDetailPresenter;

    private ImageView mUserDetailImage;
    private TextView mUserDetailName;
    private TextView mGenderText;
    private TextView mEmailText;
    private TextView mBirthdateText;
    private TextView mAddressText;

    public static Intent newIntent(Context context, User user){
        Intent intent = new Intent(context, UserDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(User.USER_KEY, user);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        mUserDetailPresenter = new UserDetailPresenter(this);

        mUserDetailImage = (ImageView) findViewById(R.id.user_detail_image);
        mUserDetailName = (TextView) findViewById(R.id.user_detail_name);
        mGenderText = (TextView) findViewById(R.id.user_gender);
        mEmailText = (TextView) findViewById(R.id.user_email);
        mBirthdateText = (TextView) findViewById(R.id.user_birthday);
        mAddressText = (TextView) findViewById(R.id.user_address);

        mUserDetailPresenter.displayUserData(getIntent().getParcelableExtra(User.USER_KEY));
    }

    @Override
    public void onDetailDisplayed(User user) {

        Picture picture = user.getPicture();

        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        Picasso.with(this).load(picture.getLargePictureUrl())
                .placeholder(R.drawable.placeholder_user_image)
                .fit().centerCrop()
                .transform(transformation)
                .into(mUserDetailImage);

        mUserDetailName.setText(getString(R.string.user_name, user.getName().getTitle(),
                user.getName().getFirst(), user.getName().getLast()));
        mGenderText.setText(user.getGender());
        mEmailText.setText(user.getEmail());
        mBirthdateText.setText(FormatUtils.getFormattedBirthdate(this, user));
        mAddressText.setText(FormatUtils.getFormattedLocation(this, user.getLocation()));
    }
}

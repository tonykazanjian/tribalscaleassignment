package com.tonykazanjian.tribalscaleassignment.user_detail;

import com.tonykazanjian.tribalscaleassignment.model.User;

/**
 * @author Tony Kazanjian
 */

public interface UserDetailView {
    void onDetailDisplayed(User user);
}

package com.tonykazanjian.tribalscaleassignment.user_detail;

import com.tonykazanjian.tribalscaleassignment.model.User;

/**
 * @author Tony Kazanjian
 */

public class UserDetailPresenter {

    UserDetailView mUserDetailView;

    public UserDetailPresenter(UserDetailView userDetailView) {
        mUserDetailView = userDetailView;
    }

    public void displayUserData(User user){
        mUserDetailView.onDetailDisplayed(user);
    }
}

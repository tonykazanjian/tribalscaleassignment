package com.tonykazanjian.tribalscaleassignment;

import android.support.test.espresso.IdlingResource;

/**
 * @author Tony Kazanjian
 */

public class ActivityIdlingResource implements IdlingResource{

    private IdlingResource.ResourceCallback mResourceCallback;


    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public boolean isIdleNow() {

        return BaseActivity.mIsResponseLoaded;
    }

    @Override
    public void registerIdleTransitionCallback(IdlingResource.ResourceCallback callback) {
        mResourceCallback = callback;
    }

    public void setIdleState(boolean isIdle) {
        BaseActivity.mIsResponseLoaded = isIdle;
        if (isIdle) {
            mResourceCallback.onTransitionToIdle();
        }
    }
}

package com.tonykazanjian.tribalscaleassignment;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;

/**
 * @author Tony Kazanjian
 */

public class BaseActivity extends AppCompatActivity {

    public static ActivityIdlingResource mActivityIdlingResource;
    public static boolean mIsResponseLoaded;

    /**
     * Only called from test, creates and returns a new {@link ActivityIdlingResource}.
     */
    @VisibleForTesting
    @NonNull
    public static IdlingResource getIdlingResource() {
        if (mActivityIdlingResource == null) {
            mActivityIdlingResource = new ActivityIdlingResource();
        }
        return mActivityIdlingResource;
    }

    public static void setActivityIdle() {
        if (mActivityIdlingResource != null) {
            mActivityIdlingResource.setIdleState(true);
        }
    }
}

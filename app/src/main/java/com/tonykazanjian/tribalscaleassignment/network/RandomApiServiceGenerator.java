package com.tonykazanjian.tribalscaleassignment.network;

import android.support.annotation.NonNull;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tonykazanjian.tribalscaleassignment.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Tony Kazanjian
 */

public class RandomApiServiceGenerator {

    private static OkHttpClient sTokenAuthHttpClient;
    private static RandomApiService sRandomApiService;

    public static String getApiBaseUrl() {
        return Constants.SERVER_BASE_URL;
    }

    public static GsonBuilder getDefaultGsonBuilder() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(Constants.UTC_DATE_FORMAT);

        return gsonBuilder;
    }

    public static Retrofit.Builder getRetrofitBuilder(@NonNull Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(getApiBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    public static RandomApiService createService() {
        if (sTokenAuthHttpClient == null) {
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.addNetworkInterceptor(new StethoInterceptor());

            httpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();

                    // Assuming query parameters would be constant each time we make the call
                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("results", Constants.QUERY_USER_SIZE)
                            .addQueryParameter("nat", Constants.QUERY_USER_NATIONALITY)
                            .build();

                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);                }
            });


            sTokenAuthHttpClient = httpClientBuilder.build();

            sTokenAuthHttpClient = httpClientBuilder.readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS).build();
            Retrofit retrofit = getRetrofitBuilder(getDefaultGsonBuilder().create()).client(sTokenAuthHttpClient).build();

            if(sRandomApiService == null) {
                sRandomApiService = retrofit.create(RandomApiService.class);
            }
        }

        return sRandomApiService;
    }

}

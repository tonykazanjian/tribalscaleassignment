package com.tonykazanjian.tribalscaleassignment.network;

import com.tonykazanjian.tribalscaleassignment.model.User;
import com.tonykazanjian.tribalscaleassignment.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Tony Kazanjian
 */

public interface RandomApiService {

    @GET("/api")
    Call<UserResponse> getUsers();
}

package com.tonykazanjian.tribalscaleassignment;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * @author Tony Kazanjian
 */

public class TribalScaleAssignmentApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }
}

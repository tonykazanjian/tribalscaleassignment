package com.tonykazanjian.tribalscaleassignment.utils;

/**
 * @author Tony Kazanjian
 */

public class Constants {
    public static final String SERVER_BASE_URL = "https://randomuser.me";
    public static final String UTC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String QUERY_USER_SIZE = "10";
    public static final String QUERY_USER_NATIONALITY = "us";
}

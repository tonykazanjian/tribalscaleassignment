package com.tonykazanjian.tribalscaleassignment.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.tonykazanjian.tribalscaleassignment.network.RandomApiService;
import com.tonykazanjian.tribalscaleassignment.network.RandomApiServiceGenerator;

/**
 * @author Tony Kazanjian
 */

public class NetworkUtils {

    /**
     *
     * Determine if We Have an Internet Connection
     * @return boolean
     */
    public static boolean isConnectedToInternet(Context context){

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static RandomApiService getService(Context context){
        if (NetworkUtils.isConnectedToInternet(context)){
            return RandomApiServiceGenerator.createService();
        } else return null;
    }
}

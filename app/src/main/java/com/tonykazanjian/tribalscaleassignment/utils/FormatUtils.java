package com.tonykazanjian.tribalscaleassignment.utils;

import android.content.Context;

import com.tonykazanjian.tribalscaleassignment.R;
import com.tonykazanjian.tribalscaleassignment.model.Location;
import com.tonykazanjian.tribalscaleassignment.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Tony Kazanjian
 */

public class FormatUtils {

    private static final String API_DATE_FORMAT = "yyyy-MM-dd";
    private static final String YEAR = "yyyy";
    private static final String MONTH = "MMMM";
    private static final String DAY_OF_MONTH = "d";

    public static final SimpleDateFormat SIMPLE_API_DATE_FORMATTER = new SimpleDateFormat(API_DATE_FORMAT, Locale.US);
    public static final SimpleDateFormat SINGLE_DAY_FORMAT =
            new SimpleDateFormat(DAY_OF_MONTH, Locale.US);
    public static final SimpleDateFormat MONTH_NAME_FORMAT =
            new SimpleDateFormat(MONTH, Locale.US);
    public static final SimpleDateFormat YEAR_FORMAT =
            new SimpleDateFormat(YEAR, Locale.US);


    public static String getFormattedLocation(Context context, Location location) {
        String firstLine = location.getStreet();
        String secondLine = location.getCity() + ", " + location.getState() + " " + location.getPostCode();
        return context.getString(R.string.user_address, firstLine, secondLine);
    }

    public static String getMonthName(final Date date) {
        return MONTH_NAME_FORMAT.format(date);
    }

    public static String getDayOfMonth (final Date date) {
        String dayString = SINGLE_DAY_FORMAT.format(date);

        return removeZero(dayString);
    }

    public static String getYear(final Date date){
        return YEAR_FORMAT.format(date);
    }

    public static String getFormattedBirthdate(Context context, User user){
        Date date = null;
        try {
            date = SIMPLE_API_DATE_FORMATTER.parse(user.getBirthday());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return context.getString(R.string.user_birthdate, getMonthName(date), getDayOfMonth(date), getYear(date));
    }

    public static String removeZero(String dateString) {
        StringBuilder sb = new StringBuilder(dateString);

        if (dateString.substring(0,1).equals("0")){
            sb.deleteCharAt(0);
        }
        return sb.toString();
    }
}

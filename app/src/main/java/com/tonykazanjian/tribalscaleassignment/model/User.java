package com.tonykazanjian.tribalscaleassignment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author Tony Kazanjian
 */

public class User implements Parcelable {

    public static final String USER_KEY = "user_key";

    @SerializedName("gender")
    private String mGender;

    @SerializedName("name")
    private Name mName;

    @SerializedName("location")
    private Location mLocation;

    @SerializedName("picture")
    private Picture mPicture;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("dob")
    private String mBirthday;

    public String getGender() {
        return mGender;
    }

    public Name getName() {
        return mName;
    }

    public Location getLocation() {
        return mLocation;
    }

    public Picture getPicture() {
        return mPicture;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public void setName(Name name) {
        mName = name;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public void setPicture(Picture picture) {
        mPicture = picture;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void setBirthday(String birthday) {
        mBirthday = birthday;
    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mGender);
        dest.writeParcelable(this.mName, flags);
        dest.writeParcelable(this.mLocation, flags);
        dest.writeParcelable(this.mPicture, flags);
        dest.writeString(this.mEmail);
        dest.writeString(this.mBirthday);
    }

    protected User(Parcel in) {
        this.mGender = in.readString();
        this.mName = in.readParcelable(Name.class.getClassLoader());
        this.mLocation = in.readParcelable(Location.class.getClassLoader());
        this.mPicture = in.readParcelable(Picture.class.getClassLoader());
        this.mEmail = in.readString();
        this.mBirthday = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}

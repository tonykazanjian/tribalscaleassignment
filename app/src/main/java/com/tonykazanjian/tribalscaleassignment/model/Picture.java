package com.tonykazanjian.tribalscaleassignment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tony Kazanjian
 */

public class Picture implements Parcelable {

    @SerializedName("large")
    private String mLargePictureUrl;

    @SerializedName("medium")
    private String mMediumPictureUrl;

    @SerializedName("thumbnail")
    private String mThumbnailUrl;


    public String getLargePictureUrl() {
        return mLargePictureUrl;
    }

    public String getMediumPictureUrl() {
        return mMediumPictureUrl;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mLargePictureUrl);
        dest.writeString(this.mMediumPictureUrl);
        dest.writeString(this.mThumbnailUrl);
    }

    public Picture() {
    }

    protected Picture(Parcel in) {
        this.mLargePictureUrl = in.readString();
        this.mMediumPictureUrl = in.readString();
        this.mThumbnailUrl = in.readString();
    }

    public static final Parcelable.Creator<Picture> CREATOR = new Parcelable.Creator<Picture>() {
        @Override
        public Picture createFromParcel(Parcel source) {
            return new Picture(source);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };
}

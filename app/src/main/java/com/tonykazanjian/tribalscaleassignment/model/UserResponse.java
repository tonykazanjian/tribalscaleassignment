package com.tonykazanjian.tribalscaleassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Tony Kazanjian
 */

public class UserResponse {
    @SerializedName("results")
    private List<User> mUserList;

    public void setUserList(List<User> userList) {
        mUserList = userList;
    }

    public List<User> getUserList() {
        return mUserList;
    }
}


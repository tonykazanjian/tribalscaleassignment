package com.tonykazanjian.tribalscaleassignment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tony Kazanjian
 */

public class Location implements Parcelable {

    @SerializedName("street")
    private String mStreet;

    @SerializedName("city")
    private String mCity;

    @SerializedName("state")
    private String mState;

    @SerializedName("postcode")
    private int mPostCode;

    public Location(String street, String city, String state, int postCode) {
        mStreet = street;
        mCity = city;
        mState = state;
        mPostCode = postCode;
    }

    public String getStreet() {
        return mStreet;
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    public int getPostCode() {
        return mPostCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mStreet);
        dest.writeString(this.mCity);
        dest.writeString(this.mState);
        dest.writeInt(this.mPostCode);
    }

    public Location() {
    }

    protected Location(Parcel in) {
        this.mStreet = in.readString();
        this.mCity = in.readString();
        this.mState = in.readString();
        this.mPostCode = in.readInt();
    }

    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel source) {
            return new Location(source);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };
}

package com.tonykazanjian.tribalscaleassignment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tony Kazanjian
 */

public class Name implements Parcelable {

    @SerializedName("title")
    private String mTitle;

    @SerializedName("first")
    private String mFirst;

    @SerializedName("last")
    private String mLast;

    public Name(String title, String first, String last) {
        mTitle = title;
        mFirst = first;
        mLast = last;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getFirst() {
        return mFirst;
    }

    public String getLast() {
        return mLast;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setFirst(String first) {
        mFirst = first;
    }

    public void setLast(String last) {
        mLast = last;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mTitle);
        dest.writeString(this.mFirst);
        dest.writeString(this.mLast);
    }

    public Name() {
    }

    protected Name(Parcel in) {
        this.mTitle = in.readString();
        this.mFirst = in.readString();
        this.mLast = in.readString();
    }

    public static final Creator<Name> CREATOR = new Parcelable.Creator<Name>() {
        @Override
        public Name createFromParcel(Parcel source) {
            return new Name(source);
        }

        @Override
        public Name[] newArray(int size) {
            return new Name[size];
        }
    };
}
